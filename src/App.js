import React from "react";
import tori from "./tori.png";
import "./App.css";
import Tori from "./Tori";

function App() {
  return (
    <div>
      <Tori />
    </div>
  );
}

export default App;
