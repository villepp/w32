import React from "react";
import "./Tori.css";

const Tori = () => {
  return (
    <div className="tori">
      <div className="tori-search">
        <input type="text" placeholder="Hakusana ja/tai postinumero" />
        <select name="what">
          <option id="1">Kaikki osastot</option>
        </select>
        <select name="where">
          <option id="1">Koko Suomi</option>
        </select>
      </div>
      <div className="tori-options">
        <label><input type="checkbox" name="how" /> Myydään</label>
        <label><input type="checkbox" name="how" /> Ostetaan</label>
        <label><input type="checkbox" name="how" /> Vuokrataan</label>
        <label><input type="checkbox" name="how" /> Halutaan vuokrata</label>
        <label><input type="checkbox" name="how" /> Annetaan</label>
        <div className="search-save-container">
          <span className="save-search">Tallenna haku</span>
          <button>Hae</button>
        </div>
      </div>
    </div>
  );
};

export default Tori;
